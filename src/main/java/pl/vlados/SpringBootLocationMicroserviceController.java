package pl.vlados;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.vlados.api.AccuweatherLocationAPI;
import pl.vlados.dto.Location;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/location/v1")
public class SpringBootLocationMicroserviceController {

    @Autowired
    Environment environment;

    //location cache
    private Map<String, Location> postalcodeLocationCache = new HashMap<>();

    @Value("${api_key}")
    private String API_KEY;

    @Autowired
    private AccuweatherLocationAPI locationAPI;

    @GetMapping("/get/by/postal-code/{postal-code}")
    public Location getLocationByPostalCode(@PathVariable(name = "postal-code") String postalCode) throws IOException, JSONException {

        String jsonStr = null;

        Location location = null;

        //callAccuWeatherAPI - true : if cache false
        boolean callApi = false;

        if (!postalCode.isEmpty()) {

            String locationKey = "";
            String title = "";
            String pCode = "";

            if (!postalcodeLocationCache.containsKey(postalCode)) {
                jsonStr = locationAPI.getLocationByPostalCode(API_KEY, postalCode);

                JSONArray jArray = new JSONArray(jsonStr);

                for (int i = 0; i < (jArray.length()); i++) {
                    JSONObject json_obj = jArray.getJSONObject(i);
                    locationKey = json_obj.getString("Key");
                    title = json_obj.getString("EnglishName");
                    pCode = json_obj.getString("PrimaryPostalCode");

                }
                if (!locationKey.isEmpty() && !title.isEmpty() && !pCode.isEmpty()) {
                    location = new Location(locationKey, title, pCode);
                    postalcodeLocationCache.put(postalCode, location);
                    callApi = true;
                }

            } else {
                location = postalcodeLocationCache.get(postalCode);
            }
        }

        if (location != null) {
            location.setPort(environment.getProperty("local.server.port"));
            location.setUseAccuweatherAPI(callApi);
        }

        return location;
    }
}

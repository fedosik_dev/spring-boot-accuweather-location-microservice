package pl.vlados.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "accuweather-service", url = "http://dataservice.accuweather.com")
public interface AccuweatherLocationAPI {

    @GetMapping("/locations/v1/postalcodes/search")
    String getLocationByPostalCode(@RequestParam(value = "apikey") String apiparam, @RequestParam (value = "q") String postalCode);
}

package pl.vlados.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Location extends Response {

    private String locationKey;
    private String locationTitle;
    private String locationPostalCode;
}

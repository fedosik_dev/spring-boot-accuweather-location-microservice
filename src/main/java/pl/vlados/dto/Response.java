package pl.vlados.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
abstract class Response {

    protected String port;
    protected boolean useAccuweatherAPI;
}
